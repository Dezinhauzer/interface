﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComConnector
{
    public partial class Form1 : Form
    {
        private ComPort _comPort;
        private Modul_I7021 _modulI7021;
        private Modul_I7044 _modulI7044;

        public Form1()
        {
            InitializeComponent();

            SetButtonEnabled(false);
        }

        public void SetButtonEnabled(bool value)
        {
            ButtonClearPort.Enabled =
                ButtonClosePort.Enabled =
                ButtonHandler.Enabled = value;
        }

        private void ButtonOpenPort_Click(object sender, EventArgs e)
        {
            _comPort = new ComPort(TextBoxComPort.Text, (int)NumericBaudRate.Value);
            var resultOpenPort = _comPort.OpenPort();
            AddLogMessage(resultOpenPort);

            _modulI7021 = new Modul_I7021(_comPort);
            _modulI7044 = new Modul_I7044(_comPort);

            if (!resultOpenPort.ToLower().Contains("ошибка")) SetButtonEnabled(true);
        }

        private void ButtonClosePort_Click(object sender, EventArgs e)
        {
            AddLogMessage(_comPort.ClosePort());
            SetButtonEnabled(false);
        }

        private void ButtonHandler_Click(object sender, EventArgs e)
        {
            var count = _modulI7044.GetCountPort(_modulI7044.GetCountPort((int)NumericNumberPort.Value));
            AddLogMessage($"С порта {NumericNumberPort.Value} считали {count}");

            if (count <= NumericFirstN.Value)
            {
                AddLogMessage(_modulI7021.SetVolt(5));
            }
            else if (count <= NumericSecondN.Value)
            {
                AddLogMessage(_modulI7021.SetVolt(2.5));
            }
        }
        private void ButtonClearPort_Click(object sender, EventArgs e)
        {
            AddLogMessage(_modulI7044.ClearCountPort((int)NumericNumberPort.Value));
        }

        private void AddLogMessage(string message)
        {
            TextBoxLog.AppendText($"{message}{Environment.NewLine}");
            TextBoxLog.ScrollToCaret();
        }

    }
}
