﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComConnector
{
    public class Modul_I7021
    {
        private ComPort _comPort;

        public Modul_I7021(ComPort comPort)
        {
            _comPort = comPort;
        }

        //  Произвести калибровку значения 10 В.
        private string SetModeVolt() => _comPort.SendPort($"${_comPort.NamePort}7");

        public string SetVolt(double volt)
        {
            SetModeVolt();
            return _comPort.SendPort($"#{_comPort.NamePort}0{volt}.000");
        }
    }
}
