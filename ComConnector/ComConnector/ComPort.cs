﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading.Tasks;

namespace ComConnector
{
    public class ComPort
    {
        private SerialPort _comPort;
        private string _namePort;
        private int _baudRate;

        public string NamePort { get => _namePort; set => _namePort = value; }

        public ComPort(string namePort, int baudRate)
        {
            _comPort = new SerialPort();
            NamePort = namePort;
            _baudRate = baudRate;
        }

        public string OpenPort()
        {
            try
            {
                // если порт был ранее открыт, закрыть его
                if (_comPort.IsOpen)
                {
                    _comPort.Close();
                }
                // название COM-порта
                _comPort.PortName = NamePort;

                // скорость работы COM-порта
                _comPort.BaudRate = _baudRate;

                // число бит данных
                _comPort.DataBits = 8;

                // число стоповых бит - один
                _comPort.StopBits = StopBits.One;

                // бит паритета - нет
                _comPort.Parity = Parity.None;

                // квитировать установление связи - нет
                _comPort.Handshake = Handshake.None;

                // число принимаемых бит
                _comPort.ReceivedBytesThreshold = 8;

                // размер буфера для записи
                _comPort.WriteBufferSize = 20;

                // размер буфера для чтения
                _comPort.ReadBufferSize = 20;

                // время таймаута чтения - по умолчанию
                _comPort.ReadTimeout = -1;

                // время таймаута записи - по умолчанию
                _comPort.WriteTimeout = -1;

                // сигнал готовности терминала к передаче данных - не
                // установлен
                _comPort.DtrEnable = false;

                // открыть порт
                _comPort.Open();

                // запрос передатчика - установлен
                _comPort.RtsEnable = true;

                // задержка
                System.Threading.Thread.Sleep(100);

                return "Порт открыт";
            }
            catch (Exception ex)
            {
                // Возвращаем ошибку
                return ReturnError(ex);
            }
        }

        public string ClosePort()
        {
            try
            {
                // освободить выходной буфер
                _comPort.DiscardOutBuffer();

                // освободить входной буфер
                _comPort.DiscardInBuffer();

                // закрыть порт
                if (_comPort.IsOpen)
                {
                    _comPort.Close();
                }

                return "Порт закрыт";
            }
            catch (Exception ex)
            {
                // Возвращаем ошибку
                return ReturnError(ex);
            }
        }

        public string SendPort(string command)
        {
            try
            {
                // записать команду в COM-порт (символ окончания команды –
                // 0x0D)
                _comPort.WriteLine(command + (char)0x0D);

                return $"Команда {command} успешно записана";
            }
            catch (Exception ex)
            {
                return ReturnError(ex);
            }
        }

        public string GetAnswer()
        {
            try
            {
                // задержка
                System.Threading.Thread.Sleep(100);

                // буфер для чтения данных из COM-порта
                byte[] dataR = new byte[_comPort.BytesToRead];

                // прочитать данные
                _comPort.Read(dataR, 0, dataR.Length);

                // Строку для результата
                StringBuilder result = new StringBuilder(String.Empty);

                // Получаем результат комнды
                dataR.ToList() // Приводим к листу
                    .ForEach(obj =>  // Вызываем Foreach - Проход по всем элементам
                    result.Append( // Присоеддиняем к текущей строоке
                    (char)obj) // Конвертируем byte в char
                );

                // Очищаем буффер
                _comPort.DiscardInBuffer();

                return $"Получен ответ: {result}";
            }
            catch (Exception ex)
            {
                return ReturnError(ex);
            }
        }

        private string ReturnError(Exception ex)
        {
            return $"Произошла ошибка: {ex.Message}";
        }
    }
}
