﻿namespace ComConnector
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NumericBaudRate = new System.Windows.Forms.NumericUpDown();
            this.TextBoxComPort = new System.Windows.Forms.TextBox();
            this.ButtonOpenPort = new System.Windows.Forms.Button();
            this.ButtonClosePort = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.NumericFirstN = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.NumericSecondN = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonHandler = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TextBoxLog = new System.Windows.Forms.TextBox();
            this.NumericNumberPort = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.ButtonClearPort = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBaudRate)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericFirstN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericSecondN)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericNumberPort)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ButtonClosePort);
            this.groupBox1.Controls.Add(this.ButtonOpenPort);
            this.groupBox1.Controls.Add(this.TextBoxComPort);
            this.groupBox1.Controls.Add(this.NumericBaudRate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.groupBox1.Location = new System.Drawing.Point(12, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 102);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Com Порт";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Адрес устройства";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Скорость устройства";
            // 
            // NumericBaudRate
            // 
            this.NumericBaudRate.Location = new System.Drawing.Point(159, 42);
            this.NumericBaudRate.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericBaudRate.Name = "NumericBaudRate";
            this.NumericBaudRate.Size = new System.Drawing.Size(181, 23);
            this.NumericBaudRate.TabIndex = 3;
            // 
            // TextBoxComPort
            // 
            this.TextBoxComPort.Location = new System.Drawing.Point(159, 16);
            this.TextBoxComPort.Name = "TextBoxComPort";
            this.TextBoxComPort.Size = new System.Drawing.Size(181, 23);
            this.TextBoxComPort.TabIndex = 4;
            // 
            // ButtonOpenPort
            // 
            this.ButtonOpenPort.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ButtonOpenPort.Location = new System.Drawing.Point(265, 71);
            this.ButtonOpenPort.Name = "ButtonOpenPort";
            this.ButtonOpenPort.Size = new System.Drawing.Size(75, 23);
            this.ButtonOpenPort.TabIndex = 5;
            this.ButtonOpenPort.Text = "Открыть";
            this.ButtonOpenPort.UseVisualStyleBackColor = true;
            this.ButtonOpenPort.Click += new System.EventHandler(this.ButtonOpenPort_Click);
            // 
            // ButtonClosePort
            // 
            this.ButtonClosePort.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ButtonClosePort.Location = new System.Drawing.Point(184, 71);
            this.ButtonClosePort.Name = "ButtonClosePort";
            this.ButtonClosePort.Size = new System.Drawing.Size(75, 23);
            this.ButtonClosePort.TabIndex = 6;
            this.ButtonClosePort.Text = "Закрыть";
            this.ButtonClosePort.UseVisualStyleBackColor = true;
            this.ButtonClosePort.Click += new System.EventHandler(this.ButtonClosePort_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonClearPort);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.NumericNumberPort);
            this.groupBox2.Controls.Add(this.ButtonHandler);
            this.groupBox2.Controls.Add(this.NumericSecondN);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.NumericFirstN);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.groupBox2.Location = new System.Drawing.Point(12, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(353, 135);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Условия задачи";
            // 
            // NumericFirstN
            // 
            this.NumericFirstN.Location = new System.Drawing.Point(159, 17);
            this.NumericFirstN.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericFirstN.Name = "NumericFirstN";
            this.NumericFirstN.Size = new System.Drawing.Size(181, 23);
            this.NumericFirstN.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "N - дополнительное 1";
            // 
            // NumericSecondN
            // 
            this.NumericSecondN.Location = new System.Drawing.Point(159, 46);
            this.NumericSecondN.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericSecondN.Name = "NumericSecondN";
            this.NumericSecondN.Size = new System.Drawing.Size(181, 23);
            this.NumericSecondN.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "N - дополнительное 2";
            // 
            // ButtonHandler
            // 
            this.ButtonHandler.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ButtonHandler.Location = new System.Drawing.Point(257, 104);
            this.ButtonHandler.Name = "ButtonHandler";
            this.ButtonHandler.Size = new System.Drawing.Size(83, 23);
            this.ButtonHandler.TabIndex = 7;
            this.ButtonHandler.Text = "Обработка";
            this.ButtonHandler.UseVisualStyleBackColor = true;
            this.ButtonHandler.Click += new System.EventHandler(this.ButtonHandler_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TextBoxLog);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.groupBox3.Location = new System.Drawing.Point(12, 271);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(353, 153);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Лог сообщений";
            // 
            // TextBoxLog
            // 
            this.TextBoxLog.Location = new System.Drawing.Point(8, 22);
            this.TextBoxLog.Multiline = true;
            this.TextBoxLog.Name = "TextBoxLog";
            this.TextBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxLog.Size = new System.Drawing.Size(331, 125);
            this.TextBoxLog.TabIndex = 0;
            // 
            // NumericNumberPort
            // 
            this.NumericNumberPort.Location = new System.Drawing.Point(159, 75);
            this.NumericNumberPort.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericNumberPort.Name = "NumericNumberPort";
            this.NumericNumberPort.Size = new System.Drawing.Size(181, 23);
            this.NumericNumberPort.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Номер порта I-7044";
            // 
            // ButtonClearPort
            // 
            this.ButtonClearPort.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ButtonClearPort.Location = new System.Drawing.Point(176, 104);
            this.ButtonClearPort.Name = "ButtonClearPort";
            this.ButtonClearPort.Size = new System.Drawing.Size(75, 23);
            this.ButtonClearPort.TabIndex = 7;
            this.ButtonClearPort.Text = "Очистить";
            this.ButtonClearPort.UseVisualStyleBackColor = true;
            this.ButtonClearPort.Click += new System.EventHandler(this.ButtonClearPort_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 435);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Программа обработки";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBaudRate)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericFirstN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericSecondN)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericNumberPort)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown NumericBaudRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxComPort;
        private System.Windows.Forms.Button ButtonClosePort;
        private System.Windows.Forms.Button ButtonOpenPort;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button ButtonHandler;
        private System.Windows.Forms.NumericUpDown NumericSecondN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NumericFirstN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TextBoxLog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NumericNumberPort;
        private System.Windows.Forms.Button ButtonClearPort;
    }
}

