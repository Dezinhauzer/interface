﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComConnector
{
    public class Modul_I7044
    {
        private ComPort _comPort;

        public Modul_I7044(ComPort comPort)
        {
            _comPort = comPort;
        }

        public int GetCountPort(int numberPort)
        {
            return int.Parse(_comPort.SendPort($"#{_comPort.NamePort}{numberPort}").Substring(3));
        }

        public string ClearCountPort(int numberPort)
        {
            return _comPort.SendPort($"${_comPort.NamePort}C{numberPort}");
        }
    }
}
